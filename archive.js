/**
 * This WebExtension grabs the URL of the page that is currently active, then
 * opens a new tab with the URL: https://web.archive.org/save/[URL]
 * 
 * --------------------------------------------------------------------------
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Open a tab pointing at Archive.org to archive a URL.
 */
function archive_url (url)
{
    // Get options from local storage
    browser.storage.local.get ().then (
        // On success
        (options) => {
            // Get the services
            let services = options  .services
                                    // One per line
                                    .split ('\n')
                                    // Filter lines
                                    .filter (service => {
                                        service = service.trim ()
                                        
                                        // Empty line or starts with # (comment)
                                        return service.length > 0 && !service.startsWith ('#');
                                    });
            
            /**
             * Create a new tab for every service.
             * Docs: https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/Tabs/create
             */
            for (let a_service of services)
                browser.tabs.create ({
                    url:    a_service + url,
                    active: options.autoswitch || false
                });
        },
        
        // On error
        (error) => { console.log ('Error: ' + error); }
    );
}

/**
 * Get the URL of the current active tab, then archive it.
 */
function archive_page ()
{
    /**
     * Select the tab that is currently active.
     * Docs: https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/Tabs/query
     *       https://developer.chrome.com/extensions/tabs
     */
    browser.tabs.query (
        {
            active: true,
            currentWindow: true
        },
        
        tabs => {
            if (!tabs || tabs.length != 1)
                return;
            
            archive_url (tabs[0].url);
        }
    );
}

/**
 * When extension starts, check that there is a
 *     {
 *       services: { }
 *     }
 * option. If there isn't, create it and set "internet_archive" as default service.
 */
browser.storage.local.get ().then (
    // On success
    (options) => {
        default_services =
            '# Internet Archive' + '\n' +
            'https://web.archive.org/save/' + '\n\n' +
            '# Archive Today' + '\n' +
            '# https://archive.today/?run=1&url=' + '\n\n' +
            '# WebCite' + '\n' +
            '# http://www.webcitation.org/archive?email=&url=' + '\n';
        
        if (!('default_services' in options))
            browser.storage.local.set ({ default_services: default_services });
        
        if (!('services' in options))
            browser.storage.local.set ({ services: default_services });
    },
    
    // On error
    (error) => { console.log ('Error: ' + error); }
);

/**
 * Add context menu to save page.
 * Docs: https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/ContextMenus/create
 */
browser.contextMenus.create ({
    id:         'archive-page',
    contexts:   [ 'page' ],
    title:      'Archive page',
    onclick:    function () { archive_page (); }
});

/**
 * Add context menu to save a selected link.
 */
browser.contextMenus.create ({
    id:         'archive-link',
    contexts:   [ 'link' ],
    title:      'Archive link',
    onclick:    function (info, tab) { archive_url (info.linkUrl); }
});

/**
 * Add context menu to save a selected image.
 */
browser.contextMenus.create ({
    id:         'archive-image',
    contexts:   [ 'image' ],
    title:      'Archive image',
    onclick:    function (info, tab) { archive_url (info.srcUrl); }
});

// Handler for toolbar icon click
browser.browserAction.onClicked.addListener (archive_page);

/**
 * Listen to command key (Ctrl+Period)
 * 
 * Docs:
 *   https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/commands
 *   https://dxr.mozilla.org/mozilla-central/source/browser/components/extensions/schemas/commands.json#14
 */
browser.commands.onCommand.addListener (function (command) {
    // The command name is defined in manifest.json
    if (command == 'command_archive')
        archive_page ();
});
