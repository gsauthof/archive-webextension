/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// API DOCS: https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/storage/local

// Update options value in local storage
function save_options (event)
{
    browser.storage.local.set ({
        autoswitch: document.querySelector ('#autoswitch').checked,
        services:   document.querySelector ('#services').value
    });
}

// Reset list of services
function reset_services ()
{
    // Retrieve list of default services
    browser.storage.local.get ().then (
        (options) => {
            // Update <textarea> content
            document.querySelector ('#services').value = options.default_services;
            
            // Update settings value
            browser.storage.local.set ({ services: options.default_services });
        }
    );
}

// Retrieve options' value from local storage
function retrieve_options ()
{
    browser.storage.local.get ().then (
        // On success
        (options) => {
            document.querySelector ('#autoswitch').checked = options.autoswitch;
            document.querySelector ('#services').value = options.services;
        },
        
        // On error
        (error) => { console.log ("Error: " + error); }
    );
}

// When "options" page load, get the options from local storage
document.addEventListener (
    'DOMContentLoaded',
    () => { retrieve_options (); }
);

// Automatically save options when one of them change
document.querySelector ('#autoswitch').addEventListener ('change', save_options);
document.querySelector ('#services').addEventListener ('input', save_options);

// Handler for "Reset services" button
document.querySelector ('#reset_services').addEventListener ('click', reset_services);
