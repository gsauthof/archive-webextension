This software is a [WebExtension](https://wiki.mozilla.org/WebExtensions) to make
archiving of web pages a little bit easier. It currently supports archiving to the
[Internet Archive](https://archive.org/web/) and to [Archive Today](https://archive.today).

# Use

This WebExtension offers four ways to archive a page:

1. Use the keyboard shortcut **Ctrl+Period**

2. Click on the toolbar icon
   
   ![Archive page](https://notabug.org/zPlus/archive-webextension/raw/master/docs/toolbar_icon.png)

3. Save page using the page context menu
    
   ![Archive page](https://notabug.org/zPlus/archive-webextension/raw/master/docs/context_menu.png)

4. Save a link using the link context menu
    
   ![Archive this link](https://notabug.org/zPlus/archive-webextension/raw/master/docs/link_menu.png)

By default, pages are archived only at the Internet Archive. To enable other
services look at the "Preferences" page.

![Preferences](https://notabug.org/zPlus/archive-webextension/raw/master/docs/preferences.png)

# Installation

Visit [addons.mozilla.org](https://addons.mozilla.org/en-US/firefox/addon/archive-webextension/)

# Development

- [WebExtensions documentation](https://developer.mozilla.org/en-US/Add-ons/WebExtensions)
- Install [web-ext](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext)
- List of Firefox's [keyboard shortcuts](https://support.mozilla.org/en-US/kb/keyboard-shortcuts-perform-firefox-tasks-quickly)

## Run/Debug

- `$ cd` to project root
- `$ web-ext run --verbose --firefox-binary=... --start-url ...`
- open "Browser Console" for logs
- no need to restart Firefox, webextension is automatically updated when making changes

## Build package (XPI)

- `$ cd` to project root
- `$ web-ext build`

## Sign

[Signing the source code is required by Mozilla](https://developer.mozilla.org/en-US/Add-ons/Distribution),
otherwise the extension will not be installed.

Mozilla will sign the app themselves before publishing it to addons.mozilla.org.

If you want to sign your own version of this extension, follow these steps:

1. Build your XPI locally (see above)
2. Create an account at [addons.mozilla.org](http://addons.mozilla.org)
3. Follow the website to upload your XPI
4. You should now be able to download the signed XPI from the website

# License

Licensed under the GNU General Public License, either version 3 or (at your option)
any later version.
